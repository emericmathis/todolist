import Task from "@/models/tasks";
import { connectToDB } from "@/utils/database";
import { NextResponse } from "next/server";

export const GET = async(request: Request) => {
    try {
        await connectToDB();

        const tasks = await Task.find();

        // Créer une réponse avec les tâches en JSON
        const response = NextResponse.json(tasks, { status: 200 });

        // Définir explicitement les en-têtes Cache-Control pour éviter la mise en cache
        response.headers.set('Cache-Control', 'no-cache, no-store, must-revalidate');

        return response;
    }
    catch (error) {
        console.log(error);
        const response = NextResponse.json("Failed to fetch all tasks" , { status: 500 });

        response.headers.set('Cache-Control', 'no-cache, no-store, must-revalidate');

        return response;
    }
}