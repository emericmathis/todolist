import Task from "@/models/tasks";
import { connectToDB } from "@/utils/database";

import { NextResponse } from "next/server";
import { IDeleteTaskRequestParams } from "@/types";

export const PATCH = async(request: Request, { params }: IDeleteTaskRequestParams) => {
    try {
        await connectToDB()

        const existingTask = await Task.findById(params.id);

        if (!existingTask) {
            const response = NextResponse.json("Task not found", { status: 404 });
            response.headers.set('Cache-Control', 'no-cache, no-store, must-revalidate');
            return response;
        }
        existingTask.completed = !existingTask.completed;
        await existingTask.save();

        const response = NextResponse.json("Task completed successfully", { status: 200 });
        response.headers.set('Cache-Control', 'no-cache, no-store, must-revalidate');
        return response;
    }
    catch (error) {
        const response = NextResponse.json("Error completing task", { status: 500});
        response.headers.set('Cache-Control', 'no-cache, no-store, must-revalidate');
        return response;
    }
}