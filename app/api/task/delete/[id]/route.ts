import Task from "@/models/tasks";
import { connectToDB } from "@/utils/database";

import { NextResponse } from "next/server";
import { IDeleteTaskRequestParams } from "@/types";

export const DELETE = async(request: Request, { params }: IDeleteTaskRequestParams) => {
    try {
        await connectToDB()

        await Task.findByIdAndDelete(params.id);

        const response = NextResponse.json("Task deleted successfully", { status: 200 });
        response.headers.set('Cache-Control', 'no-cache, no-store, must-revalidate');
        return response;
    }
    catch (error) {
        console.log(error);
        const response = NextResponse.json("Error deleting task", { status: 500});
        response.headers.set('Cache-Control', 'no-cache, no-store, must-revalidate');
        return response;
    }
}