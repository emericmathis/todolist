const fetchWrapper = async (input: RequestInfo, init?: RequestInit): Promise<Response> => {
    const defaultInit: RequestInit = {
        ...init,
        cache: 'no-store', // Applique 'no-store' globalement à toutes les requêtes pour éviter les problèmes de cache sur vercel
    };
    return fetch(input, defaultInit);
};

export default fetchWrapper;
